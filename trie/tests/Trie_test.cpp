
#include <Trie.hpp>

#include <gtest/gtest.h>

#include <iostream>
#include <string>

template<typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& list) {
    typename std::vector<T>::const_iterator it = list.begin();
    os << "[";
    if(it != list.end()) {
        os << *it++;
        for( ; it != list.end(); ++it)
            os << ", " << *it;
    }
    os << "]";
    return os;
}

TEST(Trie, find) {
    Trie<char, int> trie;

    trie.insert<std::string>("/my/path", 1);

    EXPECT_EQ(trie.find<std::string>("/my/pat").second, std::vector<int>({}));
    EXPECT_EQ(trie.find<std::string>("/my/path").second, std::vector<int>({1}));
    EXPECT_EQ(trie.find<std::string>("/my/path/subpath").second, std::vector<int>({1}));
}

TEST(Trie, insert) {
    Trie<char, int> trie;

    trie.insert<std::string>("/my/path", 1);
    trie.insert<std::string>("/my/path", 2);
    trie.insert<std::string>("/my/other/path", 3);
    trie.insert<std::string>("/", 4);
    trie.insert<std::string>("", 5);

    EXPECT_EQ(trie.find<std::string>("").second, std::vector<int>({5}));
    EXPECT_EQ(trie.find<std::string>("/").second, std::vector<int>({4}));
    EXPECT_EQ(trie.find<std::string>("/my/path/subpath").second, std::vector<int>({1,2}));
    EXPECT_EQ(trie.find<std::string>("/my/other/path").second, std::vector<int>({3}));
    EXPECT_EQ(trie.find<std::string>("/m").second, std::vector<int>({}));
}

TEST(Trie, remove) {
    Trie<char, int> trie;

    trie.insert<std::string>("/my/path", 1);
    trie.insert<std::string>("/my/path", 2);
    trie.insert<std::string>("/my/other/path", 3);
    trie.insert<std::string>("/", 4);
    trie.insert<std::string>("", 5);

    EXPECT_EQ(trie.find<std::string>("").second, std::vector<int>({5}));
    EXPECT_EQ(trie.find<std::string>("/").second, std::vector<int>({4}));
    EXPECT_EQ(trie.find<std::string>("/my/path/subpath").second, std::vector<int>({1,2}));
    EXPECT_EQ(trie.find<std::string>("/my/other/path").second, std::vector<int>({3}));
    EXPECT_EQ(trie.find<std::string>("/m").second, std::vector<int>({}));

    trie.remove<std::string>("/my/other/path");

    EXPECT_EQ(trie.find<std::string>("").second, std::vector<int>({5}));
    EXPECT_EQ(trie.find<std::string>("/").second, std::vector<int>({4}));
    EXPECT_EQ(trie.find<std::string>("/my/path/subpath").second, std::vector<int>({1,2}));
    EXPECT_EQ(trie.find<std::string>("/my/other/path").second, std::vector<int>({}));
    EXPECT_EQ(trie.find<std::string>("/m").second, std::vector<int>({}));

    trie.remove<std::string>("/my/path");

    EXPECT_EQ(trie.find<std::string>("").second, std::vector<int>({5}));
    EXPECT_EQ(trie.find<std::string>("/").second, std::vector<int>({4}));
    EXPECT_EQ(trie.find<std::string>("/my/path/subpath").second, std::vector<int>());
    EXPECT_EQ(trie.find<std::string>("/my/other/path").second, std::vector<int>({}));
    EXPECT_EQ(trie.find<std::string>("/m").second, std::vector<int>({}));

    trie.remove<std::string>("");

    EXPECT_EQ(trie.find<std::string>("").second, std::vector<int>({}));
    EXPECT_EQ(trie.find<std::string>("/").second, std::vector<int>({4}));
    EXPECT_EQ(trie.find<std::string>("/my/path/subpath").second, std::vector<int>({}));
    EXPECT_EQ(trie.find<std::string>("/my/other/path").second, std::vector<int>({}));
    EXPECT_EQ(trie.find<std::string>("/m").second, std::vector<int>({}));

    trie.remove<std::string>("/");

    EXPECT_EQ(trie.find<std::string>("").second, std::vector<int>({}));
    EXPECT_EQ(trie.find<std::string>("/").second, std::vector<int>({}));
    EXPECT_EQ(trie.find<std::string>("/my/path/subpath").second, std::vector<int>({}));
    EXPECT_EQ(trie.find<std::string>("/my/other/path").second, std::vector<int>({}));
    EXPECT_EQ(trie.find<std::string>("/m").second, std::vector<int>({}));
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv); 
    return RUN_ALL_TESTS();
}