
#pragma once

#include <cassert>

#include <vector>
#include <algorithm>
#include <functional>

template<typename Key, typename Value, typename KeyCompare = std::less<Key>, typename ValueCompare = std::less<Value> >
class Trie
{
    public:
        typedef std::vector<Value> ValueList;

    private:
        class Node {
            private:
                typedef std::vector<Node*> NodeList;
                typedef typename NodeList::iterator NodeIter;
                typedef typename NodeList::const_iterator ConstNodeIter;
                typedef std::pair<NodeIter, NodeIter> NodeRange;
                typedef std::pair<ConstNodeIter, ConstNodeIter> ConstNodeRange;

                typedef typename ValueList::iterator ValueIter;
                typedef std::pair<ValueIter, ValueIter> ValueRange;

                struct KeyComparator {
                    bool operator()(const Node* lhs, const Node* rhs) const {
                        return compare(lhs->_key, rhs->_key);
                    }
                    bool operator()(Key lhs, const Node* rhs) const {
                        return compare(lhs, rhs->_key);
                    }
                    bool operator()(const Node* lhs, Key rhs) const {
                        return compare(lhs->_key, rhs);
                    }
                    KeyCompare compare;
                };

            public:
                Node(const Key& key = Key(), Node* parent = 0)
                    : _key(key)
                    , _parent(parent)
                {}
                ~Node() {
                    NodeIter it = _children.begin();
                    for( ; it != _children.end(); ++it)
                    {
                        (*it)->_parent = 0;
                        delete *it;
                    }

                    if(_parent)
                        _parent->remove(_key);
                }
                Node* getParent() {
                    return _parent;
                }
                bool hasChildren() const {
                    return _children.empty();
                }
                size_t countChildren() const {
                    return _children.size();
                }
                Node* getOrCreate(const Key& key) {
                    NodeRange range = std::equal_range(_children.begin(), _children.end(), key, KeyComparator());
                    assert(std::distance(range.first, range.second) < 2);
                    return range.first == range.second ? *_children.insert(range.second, new Node(key, this)) : *range.first;
                }
                Node* get(const Key& key) {
                    NodeRange range = std::equal_range(_children.begin(), _children.end(), key, KeyComparator());
                    assert(std::distance(range.first, range.second) < 2);
                    return range.first == range.second ? 0 : *range.first;
                }
                const Node* get(const Key& key) const {
                    ConstNodeRange range = std::equal_range(_children.begin(), _children.end(), key, KeyComparator());
                    assert(std::distance(range.first, range.second) < 2);
                    return range.first == range.second ? 0 : *range.first;
                }
                const ValueList& getValues() const {
                    return _values;
                }
                void insertValue(const Value& value) {
                    _values.insert(std::upper_bound(_values.begin(), _values.end(), value, ValueCompare()), value);
                }
                void removeValue(const Value& value) {
                    ValueRange range = std::equal_range(_values.begin(), _values.end(), value, ValueCompare());
                    _values.erase(range.first, range.second);
                }
                void clearValues() {
                    _values.clear();
                }

            private:
                void remove(const Key& key) {
                    NodeRange range = std::equal_range(_children.begin(), _children.end(), key, KeyComparator());
                    assert(std::distance(range.first, range.second) < 2);
                    _children.erase(range.first, range.second);
                }

            private:
                Node* _parent;
                NodeList _children;
                ValueList _values;
                Key _key;
        };

    public:
        template<typename Iterator>
        void insert(Iterator first, Iterator last, const Value& value) {
            Node* node = &_root;
            while(first != last) {
                node = node->getOrCreate(*first);
                ++first;
            }
            node->insertValue(value);
        }

        template<typename T>
        void insert(const T& t, const Value& value) {
            insert(t.begin(), t.end(), value);
        }

        template<typename Iterator>
        std::pair<Iterator, ValueList> find(Iterator first, Iterator last) {
            Node* node = &_root;
            while(first != last) {
                Node* next = node->get(*first);
                if(!next)
                    break;
                node = next;
                ++first;
            }
            return std::make_pair(first, node->getValues());
        }

        template<typename T, typename Iterator = typename T::const_iterator>
        std::pair<Iterator, ValueList> find(const T& t) {
            return find<Iterator>(t.begin(), t.end());
        }

        template<typename Iterator>
        std::pair<Iterator, ValueList> find(Iterator first, Iterator last) const {
            const Node* node = &_root;
            while(first != last) {
                const Node* next = node->get(*first);
                if(!next)
                    break;
                node = next;
                ++first;
            }
            return std::make_pair(first, node->getValues());
        }

        template<typename T, typename Iterator = typename T::const_iterator>
        std::pair<Iterator, ValueList> find(const T& t) const {
            return find<Iterator>(t.begin(), t.end());
        }

        template<typename Iterator>
        Iterator remove(Iterator first, Iterator last) {
            Node* node = &_root;
            while(first != last) {
                Node* next = node->get(*first);
                if(!next)
                    break;
                node = next;
                ++first;
            }
            if(!node->hasChildren() && node != &_root) {
                Node* parent = node->getParent();
                while(parent && parent != &_root && parent->countChildren() == 1) {
                    node = parent;
                    parent = node->getParent();
                }
                delete node;
            } else {
                node->clearValues();
            }
            return first;
        }

        template<typename T, typename Iterator = typename T::const_iterator>
        Iterator remove(const T& t) {
            return remove<Iterator>(t.begin(), t.end());
        }

    private:
        Node _root;
};
