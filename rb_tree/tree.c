
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

enum color_t {
    RED = 0,
    BLACK,
};

const char * colorstr(enum color_t c) {
    const char * str[] = { "RED", "BLACK" };
    return str[c];
}

struct node_t {
    struct node_t * l;
    struct node_t * r;
    struct node_t * p;
    enum color_t color;
    int value;
};

struct tree_t {
    struct node_t * r;
};

struct func_t {
    struct node_t * (*insert)(struct tree_t *, int);
    void (*delete)(struct tree_t *, int);
    void (*print)(struct tree_t *);
    void (*check)(struct tree_t *);
};

void rotate_left(struct tree_t * t, struct node_t * node) {
    struct node_t * x = node, * y = x->r;
    if(x == NULL || y == NULL) return;
    x->r = y->l;
    if(y->l != NULL) y->l->p = x;
    y->p = x->p;
    if(x->p != NULL) {
        if(x->p->l == x) {
            x->p->l = y;
        } else {
            x->p->r = y;
        }
    } else {
        t->r = y;
    }
    y->l = x;
    x->p = y;
}

void rotate_right(struct tree_t * t, struct node_t * node) {
    struct node_t * x = node, * y = x->l;
    if(x == NULL || y == NULL) return;
    x->l = y->r;
    if(y->r != NULL) y->r->p = x;
    y->p = x->p;
    if(x->p != NULL) {
        if(x->p->l == x) {
            x->p->l = y;
        } else {
            x->p->r = y;
        }
    } else {
        t->r = y;
    }
    y->r = x;
    x->p = y;
}

struct node_t * insert(struct tree_t * t, int val) {
    struct node_t ** pp = &t->r, *p = *pp, * pr = NULL;;
    while(p != NULL) {
        if(val == p->value) return NULL;
        pp = (p->value > val) ? &(p->l) : &(p->r);
        pr = p;
        p = *pp;
    }
    p = calloc(1, sizeof(*p));
    assert(p);
    p->value = val;
    p->color = RED;
    p->p = pr;
    *pp = p;
    return p;
}

struct node_t * insert2(struct tree_t * t, int val) {
    struct node_t * p = insert(t, val), * x = p, * y;
    if(p == NULL) return p;
    while(x != t->r && x->p->color == RED) {
        if(x->p == x->p->p->l) {
            y = x->p->p->r;
            if(y != NULL && y->color == RED) {
                puts("insert case 1");
                y->color = BLACK;
                x->p->color = BLACK;
                x = x->p->p;
                x->color = RED;
            } else {
                if(x == x->p->r) {
                    puts("insert case 2");
                    x = x->p;
                    rotate_left(t, x);
                }
                puts("insert case 3");
                x->p->color = BLACK;
                x->p->p->color = RED;
                rotate_right(t, x->p->p);
            }
        } else {
            y = x->p->p->l;
            if(y != NULL && y->color == RED) {
                puts("insert case 4");
                y->color = BLACK;
                x->p->color = BLACK;
                x = x->p->p;
                x->color = RED;
            } else {
                if(x == x->p->l) {
                    puts("insert case 5");
                    x = x->p;
                    rotate_right(t, x);
                }
                puts("insert case 6");
                x->p->color = BLACK;
                x->p->p->color = RED;
                rotate_left(t, x->p->p);
            }
        }
    }
    t->r->color = BLACK;
    return p;
}

struct node_t * min(struct node_t * n) {
    if(n == NULL) return NULL;
    if(n->l != NULL) return min(n->l);
    return n;
}

struct node_t * max(struct node_t * n) {
    if(n == NULL) return NULL;
    if(n->r != NULL) return max(n->r);
    return n;
}

struct node_t * search(struct node_t * n, int val) {
    if(n == NULL) return NULL;
    if(n->value > val) {
        return search(n->l, val);
    } else if(n->value < val) {
        return search(n->r, val);
    } else {
        return n;
    }
}

struct node_t * sibling(struct node_t * n) {
    if(n == NULL || n->p == NULL) return NULL;
    return (n->p->l == n) ? n->p->r : n->p->l;
}

void delete2_case6(struct tree_t * t, struct node_t * n) {
    struct node_t * s = sibling(n);
    puts("delete case 6");
    if(s != NULL) {
        s->color = s->p->color;
        s->p->color = BLACK;
        if(s->p->r == s) {
            s->r->color = BLACK;
            rotate_left(t, s->p);
        } else {
            s->l->color = BLACK;
            rotate_right(t, s->p);
        }
    }
}

void delete2_case5(struct tree_t * t, struct node_t * n) {
    struct node_t * s = sibling(n);
    puts("delete case 5");
    if(s != NULL && s->color == BLACK) {
        if(s->p->r == s && s->r->color == BLACK && s->l->color == RED) {
            s->color = RED;
            s->l->color = BLACK;
            rotate_right(t, s);
        } else if(s->p->l == s && s->l->color == BLACK && s->r->color == RED) {
            s->color = RED;
            s->r->color = BLACK;
            rotate_left(t, s);
        }
    }
    delete2_case6(t, n);
}

void delete2_case4(struct tree_t * t, struct node_t * n) {
    struct node_t * s = sibling(n);
    puts("delete case 4");
    if(s != NULL && s->color == BLACK && s->p->color == RED && (s->l == NULL || s->l->color == BLACK) && (s->r == NULL || s->r->color == BLACK)) {
        s->color = RED;
        s->p->color = BLACK;
    } else {
        delete2_case5(t, n);
    }
}

void delete2_case1(struct tree_t * t, struct node_t * n);

void delete2_case3(struct tree_t * t, struct node_t * n) {
    struct node_t * s = sibling(n);
    puts("delete case 3");
    if(s != NULL && s->color == BLACK && s->p->color == BLACK && (s->l == NULL || s->l->color == BLACK) && (s->r == NULL || s->r->color == BLACK)) {
        s->p->color = RED;
        delete2_case1(t, n);
    } else {
        delete2_case4(t, n);
    }
}

void delete2_case2(struct tree_t * t, struct node_t * n) {
    struct node_t * s = sibling(n);
    puts("delete case 2");
    if(s != NULL && s->color == RED) {
        s->p->color = RED;
        s->color = BLACK;
        if(s->p->l == s) {
            rotate_left(t, s->p);
        } else {
            rotate_right(t, s->p);
        }
    }
    delete2_case3(t, n);
}

void delete2_case1(struct tree_t * t, struct node_t * n) {
    puts("delete case 1");
    if(n->p == NULL) {
        n->color = BLACK;
    } else {
        delete2_case2(t, n);
    }
}

void delete(struct tree_t * t, int val) {
    struct node_t * n = search(t->r, val);
    if(n != NULL) {
        struct node_t * c;
        if(n->l != NULL && n->r != NULL) {
            c = min(n->r);
            n->value = c->value;
            n = c;
        }
        c = n->l != NULL ? n->l : n->r;
        if(c != NULL) {
            c->p = n->p;
        }
        if(n->p != NULL) {
            if(n->p->l == n) {
                n->p->l = c;
            } else {
                n->p->r = c;
            }
        } else {
            t->r = c;
        }
        free(n);
    } else {
        puts("not found");
    }
}

void delete2(struct tree_t * t, int val) {
    struct node_t * n = search(t->r, val);
    if(n != NULL) {
        struct node_t * c;
        if(n->l != NULL && n->r != NULL) {
            c = min(n->r);
            n->value = c->value;
            n = c;
        }
        c = n->l != NULL ? n->l : n->r;
        if(c != NULL) {
            c->p = n->p;
            if(c->color == BLACK && n->color == BLACK) {
                delete2_case1(t, c);
            } else {
                c->color = BLACK;
            }
        } else if(n->color == BLACK) {
            delete2_case1(t, n);
        }
        if(n->p != NULL) {
            if(n->p->l == n) {
                n->p->l = c;
            } else {
                n->p->r = c;
            }
        } else {
            t->r = c;
        }
        free(n);
    } else {
        puts("not found");
    }
}

int check_black_depth(struct node_t * n) {
   int l, r;
   if(n == NULL) return 1;
   l = check_black_depth(n->l);
   r = check_black_depth(n->r);
   if(l != r) {
        printf("depth differs at value %d\n", n->value);
   }
   return (n->color == BLACK ? 1 : 0) + r;
}

void check_colors(struct node_t * n) {
    if(n == NULL) return;
    if(n->color == RED) {
        if(n->p && n->p->color == RED) {
            printf("wrong color at value %d\n", n->value);
        }
    }
    check_colors(n->l);
    check_colors(n->r);
}

void check(struct tree_t * t) {

}

void check2(struct tree_t * t) {
    check_colors(t->r);
    check_black_depth(t->r);
}

void print_helper(struct node_t * n, int indent) {
    if(n == NULL) return;
    print_helper(n->r, indent + 2);
    printf("%*c%d\n", indent, ' ', n->value);
    print_helper(n->l, indent + 2);
}

void print(struct tree_t * t) {
    print_helper(t->r, 0);
}

void print2_helper(struct node_t * n, int indent) {
    if(n == NULL) return;
    print2_helper(n->r, indent + 2);
    printf("%*c%d (%s)\n", indent, ' ', n->value, colorstr(n->color));
    print2_helper(n->l, indent + 2);
}

void print2(struct tree_t * t) {
    print2_helper(t->r, 0);
}

int main(int argc, char ** argv) {
    char buffer[20];
    struct tree_t t = {0};
    struct func_t f;
    int v = 0;

    printf("Select mode:\n1 - binary tree\n2 - rb tree\n");
    while(v == 0 && fgets(buffer, sizeof(buffer), stdin) != NULL) {
        v = atoi(buffer);
        switch(v) {
            case 1:
                f.insert = &insert;
                f.delete = &delete;
                f.print = &print;
                f.check = &check;
                break;
            case 2:
                f.insert = &insert2;
                f.delete = &delete2;
                f.print = &print2;
                f.check = &check2;
                break;
            default:
                puts("unknown mode");
                v = 0;
                break;
        }
    }
    printf("Commands:\ni<int> - insert a value\nd<int> - delete a value\n");
    while(fgets(buffer, sizeof(buffer), stdin) != NULL) {
        if(sscanf(buffer, "i%d", &v) == 1) {
            f.insert(&t, v);
        } else if(sscanf(buffer, "d%d", &v) == 1) {
            f.delete(&t, v);
        }
        printf("----------------------\n");
        f.print(&t);
        f.check(&t);
        printf("----------------------\n");
    }
    return 0;
};
